-module(mysql_publish_test).
-export([handle_usual_test/0, handle_rpc_test/1]).
%date of publish_history_storage-args is the time when the mqtt message is published
-include("publish_history_storage.hrl").


handle_usual_test() ->
  {ok, Mysql_pid} = application:get_env(publish_history_storage, mysql_pid),
  ok = mysql:query(Mysql_pid, <<"delete from publish_history_storage where msgid = 'test_mysql_publish'">>),
  Mysql_publish_args = #message{msgid = <<"test_mysql_publish">>, topic = <<"test_topic">>, payload = <<"test_payload">>, date = <<"test_date">>},
  publish_history_storage:store(Mysql_publish_args),
  {ok, Colunm, Rows} = mysql:query(Mysql_pid, <<"select * from publish_history_storage where msgid = 'test_mysql_publish'">>),
  [<<"msgid">>, <<"topic">>, <<"payload">>, <<"date">>] = Colunm,
  [[<<"test_mysql_publish">>,<<"test_topic">>,<<"test_payload">>,<<"test_date">>]] = Rows,
  ok = mysql:query(Mysql_pid, <<"delete from publish_history_storage where msgid = 'test_mysql_publish'">>),
  'local test passed'.

handle_rpc_test(Node) ->
  pong = net_adm:ping(Node),
%  Mysql_publish_args = #message{msgid = <<"test_mysql_publish">>, topic = <<"test_topic">>, payload = <<"test_payload">>, date = <<"test_date">>},
  
  {ok, Mysql_config} = rpc:call(Node, application, get_env, [publish_history_storage, mysql_connection]),
  
  Host = proplists:get_value(host, Mysql_config),
  User = proplists:get_value(user, Mysql_config),
  Database = proplists:get_value(database, Mysql_config),
  Password = proplists:get_value(password, Mysql_config),
%  rpc:cast(Node, publish_history_storage, store, [Mysql_publish_args, Mysql_login_information]),
  {ok, _Mysql_pid} = rpc:call(Node, mysql, 'start_link', [[{host, Host},{user, User},{password, Password},{database, Database}]]),
%Re =  rpc:call(Node, mysql, 'query', [_Mysql_pid, <<"select * from publish_history_storage where msgid = 'test_mysql_publish'">>]),
 % [<<"msgid">>, <<"topic">>, <<"payload">>, <<"date">>] = Colunm,
 % [[<<"test_mysql_publish">>,<<"test_topic">>,<<"test_payload">>,<<"test_date">>]] = Rows,
%Re = rpc:call(Node, mysql, 'query', [Mysql_pid, <<"delete from publish_history_storage where msgid = 'test_mysql_publish'">>]),

  'rpc test passed'.