%%%-------------------------------------------------------------------
%%% @author XuLei
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. 八月 2016 19:06
%%%-------------------------------------------------------------------
-module(publish_history_storage).
-author("XuLei").

-include("publish_history_storage.hrl").

-behaviour(gen_server).

%% API
-export([start_link/0, store/1]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {}).

-compile({parse_transform, lager_transform}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
%% -spec(start_link() ->
%%   {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
%% -spec(init(Args :: term()) ->
%%    {ok, State :: #state{}} |
%%    {ok, State :: #state{}, timeout() | hibernate} |
%%    {stop, Reason :: term()} | ignore).
init([]) ->
  {ok, Mysql_Args} = application:get_env(publish_history_storage, mysql_connection),
  lager:debug("mysql args: ~p~n", [Mysql_Args]),
  {ok, Mysql_config} = application:get_env(publish_history_storage, mysql_connection),
  Host = proplists:get_value(host, Mysql_config),
  User = proplists:get_value(user, Mysql_config),
  Database = proplists:get_value(database, Mysql_config),
  Password = proplists:get_value(password, Mysql_config),
  {ok, Mysql_pid} = mysql:start_link([{host, Host},{user, User},{password, Password},{database, Database}]),
  ok = application:set_env(publish_history_storage, mysql_pid, Mysql_pid),
  {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call(Message, _From, State) ->
  Message_after_process = process_message(Message),
  {ok, Mysql_pid} = application:get_env(publish_history_storage, mysql_pid),
  ok = mysql:query(Mysql_pid,<<"create table if not exists publish_history_storage(msgid varbinary(1000), topic varbinary(1000), payload varbinary(3000), date varbinary(1000))">>),
  ok = mysql:query(Mysql_pid, <<"insert publish_history_storage value(?,?,?,?)">>, [Message_after_process#message.msgid, Message_after_process#message.topic, Message_after_process#message.payload, Message_after_process#message.date]),
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

store(Message) ->
  gen_server:call(?SERVER, Message).

process_message(Message) ->
  Msgid = case is_binary(Message#message.msgid) of
            true ->  Message#message.msgid;
            false -> <<"not_binary">>
          end,
  Topic = case is_binary(Message#message.topic) of
            true ->  Message#message.topic;
            false -> <<"not_binary">>
          end,
  Payload = case is_binary(Message#message.payload) of
              true ->  Message#message.payload;
              false ->  <<"not_binary">>
            end,
  Date = case is_binary(Message#message.date) of
           true ->  Message#message.date;
           false ->  <<"not_binary">>
         end,
  Message_after_process = #message{msgid = Msgid, topic = Topic, payload = Payload, date = Date},
  Message_after_process.