%%%-------------------------------------------------------------------
%%% @author XuLei
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 15. 八月 2016 19:26
%%%-------------------------------------------------------------------
-module(publish_history_storage_sup).
-author("Xulei").

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([]) ->
  Child = {
    publish_history_storage_sup,
    {publish_history_storage, start_link, []},
    permanent,
    5000,
    worker,
    [publish_history_storage]
    },
  {ok, {{one_for_one, 5,10}, [Child]}}.